/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketassinaturadigital;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.PublicKey;
import java.util.Date;
import java.util.Properties;
import model.PublicKeyStream;

/**
 *
 * @author Matheus
 */
public class SocketAssinaturaDigital implements Runnable {

    public Socket cliente;
    private static Properties prop;
    private static String caminhoRaiz;
    private static int porta;

    public SocketAssinaturaDigital(Socket cliente) {
        this.cliente = cliente;
    }

    /**
     * Carrega o arquivo de Propriedades para transferência de arquivos.
     *
     * @return
     * @throws IOException
     */
    public static Properties getProp() throws IOException {
        Properties props = new Properties();
        InputStream is = SocketAssinaturaDigital.class.getClass().getResourceAsStream("/propriedades/socketAssinaturaDigital.properties");

        if (is == null) {
            new Exception("Arquivo não encontrado.");
        }

        props.load(is);
        return props;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here

        prop = getProp();
        caminhoRaiz = prop.getProperty("ROOT_PATH");
        porta = Integer.parseInt(prop.getProperty("PORT"));

        //Criando o servidor
        ServerSocket server_envio = new ServerSocket(porta);
        System.out.println("Socket para envio a Aplicacao aberto na porta: " + server_envio.getLocalPort());

        //Aguardando conexão do cliente
        while (true) {
            Socket cliente_envio = server_envio.accept();

            //Cria uma Thread para tratar a recepção (para envio)
            SocketAssinaturaDigital tratment = new SocketAssinaturaDigital(cliente_envio);
            Thread t = new Thread(tratment);
            t.start();
        }
    }

    @Override
    public void run() {

        InputStream socketIn = null;
        OutputStream socketOut = null;
        FileInputStream fileIn = null;
        FileOutputStream fileOut = null;
        ObjectInputStream inFromClient = null;
        ObjectOutputStream outToClient;
        int tamByte = Integer.parseInt(prop.getProperty("TAM_BYTE"));
        long init = System.currentTimeMillis();
        long diff;
        File file;
        byte[] cbuffer;
        int bytesRead;

        System.out.println("Nova conexao com o cliente: " + cliente.getInetAddress() + ":" + cliente.getLocalPort() + "\nData" + (new Date()));

        try {
            //Recebendo os parâmetros enviados pelo cliente
            PublicKeyStream pKStream;
            inFromClient = new ObjectInputStream(cliente.getInputStream());
            pKStream = (PublicKeyStream) inFromClient.readObject();

            switch (pKStream.getMotivo()) {
                case "ping":
                    System.out.println("Teste realizado com sucesso.");
                    System.out.println("----------------------");

                    break;
                case "download":
                    try {
                        //Criando tamanho de leitura
                        cbuffer = new byte[tamByte];
                        //Criando arquivo que será transmitido pelo servidor

                        System.out.println("Lendo o arquivo...");
                        String caminho = prop.getProperty("CHAVES_PATH") + pKStream.getNomeArquivo();
                        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(caminho));
                        PublicKey chavePublica = (PublicKey) inputStream.readObject();

                        //Cria o objeto para enviar ao cliente
                        PublicKeyStream documento = new PublicKeyStream(pKStream.getNomeArquivo(), chavePublica);

                        outToClient = new ObjectOutputStream(cliente.getOutputStream());

                        //Envia ao cliente o documento a ser recuperado
                        outToClient.writeObject(documento);

                        System.out.println("Arquivo enviado.");
                        System.out.println("----------------------");

                        break;
                    } catch (Exception ex) {
                        System.out.println("SocketAssinaturaServer: Erro ao enviar arquivo para a aplicacao: " + ex.getMessage());
                    }
                case "upload":
                    try {

                        File chavePublicaFile = new File(prop.getProperty("CHAVES_PATH") + pKStream.getNomeArquivo());

                        pKStream.setCaminhoChave(chavePublicaFile.getAbsolutePath());

                        if (chavePublicaFile.getParentFile() != null) {
                            chavePublicaFile.getParentFile().mkdirs();
                        }

                        chavePublicaFile.createNewFile();
                        // Salva a Chave Pública no arquivo
                        ObjectOutputStream chavePublicaOS = new ObjectOutputStream(
                                new FileOutputStream(chavePublicaFile));
                        chavePublicaOS.writeObject(pKStream.getChavePublica());
                        chavePublicaOS.close();

                        //Cria o objeto stream para enviar ao Cliente (o caminho final do arquivo)
                        System.out.println("Retornando caminho final do arquivo...");
                        outToClient = new ObjectOutputStream(cliente.getOutputStream());
                        System.out.println("Caminho final retornado!");

                        //Cria o objeto para enviar ao cliente
                        PublicKeyStream documento = new PublicKeyStream("Nome do arquivo", pKStream.getNomeArquivo());

                        //Envia ao servidor o documento a ser recuperado
                        outToClient.writeObject(documento);
                        System.out.println("----------------------");

                        break;

                    } catch (Exception ex) {
                        System.out.println("SocketAssinaturaDigital: Erro ao receber arquivo da aplicacao: " + ex.getMessage());
                    }
                default:
                    throw new Exception("Não foi repassado nenhum parametro de transmissao!");
            }
        } catch (Exception ex) {
            System.out.println("Erro: " + ex.getMessage());
        } finally {
            if (this.cliente != null) {
                try {
                    this.cliente.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação com cliente: " + ex.getMessage());
                }
            }
            if (socketOut != null) {
                try {
                    socketOut.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação OutputStream: " + ex.getMessage());
                }
            }
            if (fileIn != null) {
                try {
                    fileIn.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação FileInputStream: " + ex.getMessage());
                }
            }
            if (inFromClient != null) {
                try {
                    inFromClient.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação ObjectInputStream: " + ex.getMessage());
                }
            }
            if (socketIn != null) {
                try {
                    socketIn.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação InputStream: " + ex.getMessage());
                }
            }
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (Exception ex) {
                    System.out.println(" Erro ao fechar canal de comunicação FileOutputStream: " + ex.getMessage());
                }
            }
        }
    }
}
